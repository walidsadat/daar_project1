## Introduction

This project is an implementation of a *clone* of the *UNIX* **_egrep_** command as part of the *Master 2 - STL* course *DAAR* at *Sorbonne Université* by Amine Benslimane and Walid Sadat.

## Run
To run the command, run the **_runEgrep.sh_** *shell script* passing a regular expression and a valid file name as arguments :

```sh
sh runEgrep.sh RegEx FileName
```

Or by using *bash* command :

```sh
bash runEgrep.sh RegEx FileName
```

Or by setting permission on the script using *chmod* command :

```sh
chmod +x runEgrep.sh
./runEgrep RegEx FileName
```
